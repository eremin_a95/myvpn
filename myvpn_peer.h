#ifndef _MYVPN_PEER_H
#define _MYVPN_PEER_H

#include <linux/ip.h>
#include <net/ip.h>
#include <linux/hashtable.h>

#include "myvpn_queueing.h"
#include "myvpn_working.h"
#include "myvpn_crypto.h"

#ifdef MYVPN_DEBUG
#define MYVPN_PRINT_PEER(peer)\
	printk_ratelimited(KERN_INFO	"MyVPN: Peer: ID = %d\n"\
									"             Addr = %d.%d.%d.%d:%d\n"\
									"             Listen on port = %d\n"\
									"             MTU = %d\n",\
		be32_to_cpu((peer)->id),\
		((u8 *)(&(peer)->addr))[0], ((u8 *)(&(peer)->addr))[1],\
		((u8 *)(&(peer)->addr))[2], ((u8 *)(&(peer)->addr))[3],\
		be16_to_cpu((peer)->peer_port),\
		be16_to_cpu((peer)->local_port),\
		(peer)->mtu);
#else
#define MYVPN_PRINT_PEER(peer)
#endif

enum {
	MYVPN_CLIENT = 0,
	MYVPN_CRYPTO_GW = 1,
};

struct addr_hnode_container;

struct myvpn_peer
{
	unsigned long				index;
	__be32						id;
	u8							type;
	struct in_addr				access_addr;
	int							r_addr_num;
	struct in_addr				* r_addrs;
	/* struct in_addr 			* v_addrs; */
	int							tun_num;
	struct in_addr				* tunnels;
	__be16						peer_port;
	__be16						local_port;
	int							mtu;
	struct addr_hnode_container	* addr_hnode_ct;
	struct hlist_node			id_hnode;
	struct myvpn_crypto_ctx		* ctx_in, * ctx_out;
	struct myvpn_mc_work		* input_work, * output_work;
	struct myvpn_queue			* input_queue, * output_queue;
	struct workqueue_struct		* wq;
	/* struct flowi4 fl4 */
	/* struct hw_addr hw_addr */
	/* struct timestamp last_ts */
};

/** Inits array of peers */
int init_peers(const char * filename);

/** Uninits array of peers */
void uninit_peers(void);

/** Returns struct myvpn_peer for this host */
struct myvpn_peer * local_peer(void);

struct myvpn_peer * get_peer(unsigned long index);

/** Returns peer which from received incoming skb */
struct myvpn_peer * peer_from_myvpn_skb(struct sk_buff * skb);

/** Returns peer whom to send outgoing packet */
struct myvpn_peer * peer_from_plaintext_skb(struct sk_buff * skb);

void update_peer(	struct myvpn_peer * peer, struct sk_buff * skb);
						
bool is_skb_for_peer(struct sk_buff * skb, struct myvpn_peer * peer);

bool is_skb_for_tunnel(struct sk_buff * skb, struct myvpn_peer * peer);
						
#endif