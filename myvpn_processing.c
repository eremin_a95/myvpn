#ifndef _MYVPN_PROCESSING_C
#define _MYVPN_PROCESSING_C

#include <linux/types.h>
#include <linux/etherdevice.h>
#include <net/ip.h>

#include "myvpn_capsulating.h"
#include "myvpn_queueing.h"
#include "myvpn_working.h"

#ifdef MYVPN_DEBUG
#define MYVPN_RCV_DBG(packet)\
	printk_ratelimited(KERN_INFO "MyVPN: Received %d bytes\n",\
						(packet)->skb->len);
#define MYVPN_SND_DBG(packet)\
	printk_ratelimited(KERN_INFO "MyVPN: Sent %d bytes\n",\
						(packet)->skb->len);
#else	
#define MYVPN_RCV_DBG(packet)
#define MYVPN_SND_DBG(packet)
#endif

#define MYVPN_MSG(msg)\
	printk_ratelimited(KERN_INFO "MyVPN: %s\n", (msg));
#define MYVPN_ERR_MSG(msg, err)\
	printk_ratelimited(KERN_INFO "MyVPN: %s (error: %d)!\n", (msg), (err));


void input_process(struct work_struct * work)
{
	struct myvpn_packet * packet = NULL;
	struct myvpn_mc_container * wc = 
		container_of(work, struct myvpn_mc_container, work);
	struct myvpn_queue * input_queue = wc->queue;
	struct myvpn_mc_work * input_work = wc->owner;
	int err;
	
	queue_myvpn_finish_work(input_work);	
	while ((packet = get_first_not_processed(input_queue)) != NULL)
	{
		if (unlikely(err = decaps_packet(packet)))
		{
			delete_packet_from_queue(packet, input_queue);
			dev_kfree_skb(packet->skb);
			destroy_packet(packet);
			MYVPN_ERR_MSG("ERROR: Cannot correctly decapsulate packet!", err);
		}
		else
			packet->status = MYVPN_STATUS_READY;
	}	
	queue_myvpn_finish_work(input_work);	
	free_cpu(smp_processor_id(), input_work);	
	return;
}

void input_finish(unsigned long data)
{
	struct myvpn_packet * packet = NULL;
	struct myvpn_queue * input_queue = get_peer(data)->input_queue;
	while ((packet = dequeue_ready_packet(input_queue)) != NULL)
	{
		MYVPN_RCV_DBG(packet);
		if (packet->type == MYVPN_TO_HOST)
			netif_receive_skb(packet->skb); /*netif_rx(packet->skb);*/
			//dst_input(packet->skb);
		else if (packet->type == MYVPN_TO_TUNNEL)
			dst_output(&init_net, NULL, packet->skb);
		else
			MYVPN_MSG("ERROR: Unknown packet type!");
		destroy_packet(packet);
	}
	if (!is_queue_empty(input_queue))
		queue_myvpn_work(get_peer(data)->wq, get_peer(data)->input_work);
	return;
}

void output_process(struct work_struct * work)
{
	struct myvpn_packet * packet = NULL;
	struct myvpn_mc_container * wc = 
		container_of(work, struct myvpn_mc_container, work);
	struct myvpn_queue * output_queue = wc->queue;
	struct myvpn_mc_work * output_work = wc->owner;
	queue_myvpn_finish_work(output_work);
	while ((packet = get_first_not_processed(output_queue)) != NULL)
	{
		if (unlikely(encaps_packet(packet)))
		{
			delete_packet_from_queue(packet, output_queue);
			dev_kfree_skb(packet->skb);
			destroy_packet(packet);
			MYVPN_MSG("Cannot correctly encapsulate packet");
		}
		else
			packet->status = MYVPN_STATUS_READY;
	}
	queue_myvpn_finish_work(output_work);
	free_cpu(smp_processor_id(), output_work);
	return;
}

void output_finish(unsigned long data)
{
	struct myvpn_packet * packet = NULL;
	struct myvpn_queue * output_queue = get_peer(data)->output_queue;
	while ((packet = dequeue_ready_packet(output_queue)) != NULL)
	{
		MYVPN_SND_DBG(packet);
		dst_output(&init_net, NULL, packet->skb);
		destroy_packet(packet);
	}
	if (!is_queue_empty(output_queue))
		queue_myvpn_work(get_peer(data)->wq, get_peer(data)->output_work);
	return;
}
#endif