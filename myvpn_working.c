#include <linux/bitops.h>

#include "myvpn_working.h"

static inline
bool __is_cpu_free(int cpu, int cpu_mask)
{
	/* Free CPUs marked as 1, busy - as 0 */	
	return test_bit(cpu, (void *)&(cpu_mask));
}

static inline
void __set_cpu_busy(int cpu, int * cpu_mask)
{
	clear_bit(cpu, (void *)cpu_mask);
}

static inline
void __set_cpu_free(int cpu, int * cpu_mask)
{
	set_bit(cpu, (void *)cpu_mask);
}

static inline
void __set_cpu_mask(int num_cpu, int * cpu_mask)
{
	int i;
	*cpu_mask = 0;
	for (i = 0; i < num_cpu; i++)
		set_bit(i, (void *)cpu_mask);
}

struct myvpn_mc_work * create_myvpn_mc_work(work_func_t proc_fn,
											task_func_t finish_fn,
											struct myvpn_queue * queue,
											unsigned long data, gfp_t flags)
{
	int cur_cpu = 0, max_cpu = num_online_cpus();
	struct myvpn_mc_work * nwork = kmalloc(sizeof(struct myvpn_mc_work), flags);
	if (unlikely(!nwork))
		return NULL;
	__set_cpu_mask(max_cpu, &nwork->cpu_mask);
	nwork->proc_work = alloc_percpu_gfp(struct myvpn_mc_container, flags); /* GPL! */
	if (unlikely(!nwork->proc_work))
		goto free_and_exit;
	nwork->finish_task = kmalloc(sizeof(struct tasklet_struct), flags);
	if (unlikely(!nwork->finish_task))
		goto free_proc_and_exit;
	nwork->max_cpu = max_cpu;
	nwork->cur_cpu = cur_cpu;
	spin_lock_init(&(nwork->sl));
	for (cur_cpu = 0; cur_cpu < max_cpu; cur_cpu++)
	{
		per_cpu_ptr(nwork->proc_work, cur_cpu)->owner = nwork;
		per_cpu_ptr(nwork->proc_work, cur_cpu)->cpu = cur_cpu;
		per_cpu_ptr(nwork->proc_work, cur_cpu)->queue = queue;
		INIT_WORK(&(per_cpu_ptr(nwork->proc_work, cur_cpu)->work), proc_fn);
	}
	tasklet_init(nwork->finish_task, finish_fn, data);
	return nwork;
free_proc_and_exit:
	free_percpu(nwork->proc_work);
free_and_exit:
	kfree(nwork);
	return NULL;
}

void destroy_myvpn_mc_work(struct myvpn_mc_work * mc_work)
{
	int cur_cpu = 0, max_cpu = mc_work->max_cpu;
	tasklet_kill(mc_work->finish_task);
	free_percpu(mc_work->proc_work);
	kfree(mc_work->finish_task);
	kfree(mc_work);
}

bool queue_myvpn_work(	struct workqueue_struct * wq,
								struct myvpn_mc_work * mc_work	)
{
	if (likely(mc_work->cpu_mask))
	{
		int cur_cpu, cpu, max_cpu, cpu_mask;
		bool success = false;
		cur_cpu = mc_work->cur_cpu;
		max_cpu = mc_work->max_cpu;
		cpu = cur_cpu;
		do
		{
			cpu = (cpu + 1) % max_cpu;
			if (__is_cpu_free(cpu, mc_work->cpu_mask))
				success = queue_work_on(cpu, wq,
					&(per_cpu_ptr(mc_work->proc_work, cpu)->work));
		} while ((!success) && (cpu != cur_cpu));
		if (success)
			__set_cpu_busy(cpu, &mc_work->cpu_mask);
		mc_work->cur_cpu = cpu;	
		mc_work->max_cpu = num_online_cpus();
		return success;
	}
	return false;
}

void free_cpu(int cpu, struct myvpn_mc_work * mc_work)
{
	//spin_lock_bh(&(mc_work->sl));
	__set_cpu_free(cpu, &mc_work->cpu_mask);
	//spin_unlock_bh(&(mc_work->sl));
}