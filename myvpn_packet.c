#include <linux/ip.h>
#include <linux/list.h>
#include <linux/slab.h>

#include "myvpn_peer.h"

static struct kmem_cache * cache;

bool init_packet_allocator(void)
{
	cache = KMEM_CACHE(myvpn_packet, SLAB_HWCACHE_ALIGN);
	if (cache == NULL)
		return false;
	else
		return true;
}

void uninit_packet_allocator(void)
{
	kmem_cache_destroy(cache);
}

bool is_myvpn_skb(struct sk_buff * skb)
{
	if (likely((skb->data_len > MYVPN_OVERHEAD_SZ) ||
		((!skb->data_len) && (skb->len > MYVPN_OVERHEAD_SZ)))) 
		return (skb_myvpn_header(skb)->sign == MYVPN_SIGN);
	else
	{
		struct myvpn_header myvpn_hdr;		
		skb_copy_bits(skb, MYVPN_OVERHEAD_SZ - MYVPN_H_SZ,
						&myvpn_hdr, MYVPN_H_SZ);
		return (myvpn_hdr.sign == MYVPN_SIGN);
	}
}

bool is_from_myvpn_skb(struct sk_buff * skb)
{
	/* TODO: create function to check if skb was previously decrypted and reinjected  */
	return true;
}

struct myvpn_packet * new_packet(struct sk_buff * skb,
									struct myvpn_peer * peer,
									int status, gfp_t flags)
{ 
	/* TODO: maybe we should alloc_percpu? */
	//struct myvpn_packet * packet = kmalloc(sizeof(struct myvpn_packet), flags);
	struct myvpn_packet * packet = kmem_cache_alloc(cache, flags);
	packet->skb = skb;
	packet->peer = peer;
	packet->status = status;
	packet->type = MYVPN_UNKNOWN;
	return packet;
}

void destroy_packet(struct myvpn_packet * packet)
{
	//kfree(packet);
	kmem_cache_free(cache, packet);
}
