ccflags-y := -O3 -fvisibility=hidden
myvpn-y += myvpn_module.o myvpn_peer.o myvpn_processing.o myvpn_working.o myvpn_queueing.o myvpn_crypto.o myvpn_capsulating.o myvpn_packet.o
obj-m += myvpn.o



all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

remake: clean all

start:
	sudo ifconfig lo mtu 1500
	sudo ethtool -K enp0s3 tx off
	sudo dmesg -C
	sudo insmod myvpn.ko

stop:
	sudo ethtool -K enp0s3 tx on
	sudo rmmod myvpn.ko
	dmesg

ping:
	ping 127.0.0.1 -c 8 -s 100 -i 0.25 -p ff -w 2 &
	sleep 3
	
longping:
	ping 127.0.0.1 -c 8 -s 1600 -i 0.25 -p ff -w 2 &
	sleep 3
	
longlongping:
	ping 127.0.0.1 -c 8 -s 40000 -i 0.25 -p ff -w 2 &
	sleep 3

test: start stop

fulltest: start ping stop
	
fulltestl: start longping stop

fulltestll: start longlongping stop
	
stresstestu_clear:
	iperf3 -s -p 12321 -1 &
	sleep 2
	iperf3 -c 127.0.0.1 -p 12321 -u -O 1 -b 2500000
	sleep 2
	
stresstest_clear:
	iperf3 -s -p 12321 -1 &
	sleep 2
	iperf3 -c 127.0.0.1 -p 12321 -M 1350 -O 1 -b 100000000000
	sleep 2
	
stresstestp_clear:
	sudo ping 127.0.0.1 -s 100 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 100 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 100 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 100 -f -p ff -w 10 -q &
	sleep 12
	
stresstestpl_clear:
	sudo ping 127.0.0.1 -s 1600 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 1600 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 1600 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 1600 -f -p ff -w 10 -q &
	sleep 12
	
stresstestpll_clear:
	sudo ping 127.0.0.1 -s 40000 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 40000 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 40000 -f -p ff -w 10 -q &
	#sudo ping 127.0.0.1 -s 40000 -f -p ff -w 10 -q &
	sleep 12
	
		
stresstestu: start stresstestu_clear stop
	
stresstest: start stresstest_clear stop

stresstestp: start stresstestp_clear stop

stresstestpl: start stresstestpl_clear stop

stresstestpll: start stresstestpll_clear stop