#include <linux/ip.h>
#include <linux/etherdevice.h>
#include <linux/inetdevice.h>
#include <linux/random.h>
#include <linux/netfilter_ipv4.h>

#include "myvpn_capsulating.h"

static u32 count = 0; /* TODO: remove */

int decaps_packet(struct myvpn_packet * packet)
{
	struct sk_buff * skb = packet->skb;
	int mac_len = skb_mac_header_len(skb);
	struct myvpn_header myvpn_hdr;
	if (unlikely(skb->len <= MYVPN_OVERHEAD_SZ))
		goto err_len;	
	update_peer(packet->peer, skb);	
	if ((!skb->data_len))
	{
		myvpn_hdr = *(skb_myvpn_header(skb));
		memcpy(	skb->data + MYVPN_OVERHEAD_SZ -  mac_len,
					skb_mac_header(skb),  mac_len );
		skb_pull(skb, MYVPN_OVERHEAD_SZ - mac_len);
		if (decrypt_and_auth(packet->peer->ctx_in, skb->data + mac_len,
								skb->data + mac_len, skb->len - mac_len,
								myvpn_hdr.cr_h.iv64, myvpn_hdr.cr_h.auth))
			return -2;
		skb->protocol = eth_type_trans(skb, skb->dev);
		skb->network_header = skb->mac_header + mac_len;  /* unnecessary? */
	}
	else
	{
		struct sk_buff * new_skb = alloc_skb(MAX_MAC_H_SZ + skb->len -
												MYVPN_OVERHEAD_SZ, GFP_ATOMIC);
		if (new_skb == NULL)
			return -ENOMEM;
		skb_reserve(new_skb, MAX_MAC_H_SZ);
		skb_copy_bits(skb, MYVPN_OVERHEAD_SZ,
						skb_put(new_skb, skb->len - MYVPN_OVERHEAD_SZ),
						skb->len - MYVPN_OVERHEAD_SZ);					
		skb_copy_bits(skb, skb_myvpn_offset(skb), &myvpn_hdr, MYVPN_H_SZ);
		if (decrypt_and_auth(packet->peer->ctx_in, new_skb->data,
								new_skb->data, new_skb->len,
								myvpn_hdr.cr_h.iv64, myvpn_hdr.cr_h.auth))
		{
			dev_kfree_skb(new_skb);
			return -2;
		}
		skb_copy_bits(skb, skb_mac_offset(skb),
						skb_push(new_skb, mac_len), mac_len);
		/* TODO: maybe something below is unnecessary? */
		skb_dst_copy(new_skb, skb);
		new_skb->queue_mapping = skb->queue_mapping;
		memcpy(&new_skb->headers_start, &skb->headers_start,
				offsetof(struct sk_buff, headers_end) -
				offsetof(struct sk_buff, headers_start));			 
		new_skb->dev = skb->dev;
		new_skb->mac_len = mac_len; /* unnecessary? */
		new_skb->protocol = eth_type_trans(new_skb, new_skb->dev);
		new_skb->ip_summed = CHECKSUM_UNNECESSARY;
		new_skb->network_header = new_skb->mac_header + mac_len;  /* unnecessary? */
		packet->skb = new_skb;
		dev_kfree_skb(skb);
	}
	skb = packet->skb;
	if (is_skb_for_peer(skb, local_peer()))
		packet->type = MYVPN_TO_HOST;
	else if (is_skb_for_tunnel(skb, local_peer()))
	{
		ip_route_me_harder(&init_net, skb, RTN_UNICAST);
		packet->type = MYVPN_TO_TUNNEL;
	}
	else
		packet->type = MYVPN_UNKNOWN;
	return 0;
err_len:
	return -1;
}

int encaps_packet(struct myvpn_packet * packet)
{
	struct sk_buff * skb = packet->skb;
	struct sk_buff * new_skb = skb;
	struct udphdr new_udp_hdr = {
		packet->peer->local_port, packet->peer->peer_port,
		cpu_to_be16(skb->len + UDP_H_SZ + MYVPN_H_SZ + MYVPN_OVERTAIL_SZ), 0
	};
	struct myvpn_header myvpn_hdr;
	struct iphdr * iph = ip_hdr(skb);
	u64 iv = get_random_u64();
	int len = skb->len;
	//if (skb_headroom(skb) < MYVPN_OVERHEAD_SZ + ETH_H_SZ + 2)
	//	if (pskb_expand_head(skb, MYVPN_OVERHEAD_SZ + ETH_H_SZ + 2, 0 , GFP_ATOMIC))
	//		return -ENOMEM;
	if ((skb->data_len) ||
					(skb_headroom(skb) < MYVPN_OVERHEAD_SZ + MAX_MAC_H_SZ))
		new_skb = skb_copy_expand(skb, MYVPN_OVERHEAD_SZ,
									MYVPN_OVERTAIL_SZ, GFP_ATOMIC);

	memset(&myvpn_hdr, 0, MYVPN_H_SZ);
	myvpn_hdr.sign = MYVPN_SIGN;
	myvpn_hdr.src_id = local_peer()->id;
	myvpn_hdr.cr_h.iv64 = iv;
	myvpn_hdr.cr_h.auth64 = 0;
	myvpn_hdr.dst_id = packet->peer->id;	
	myvpn_hdr.seq_num = __cpu_to_be32(count++); /* TODO: remove */
	//printk(KERN_INFO "MyVPN: encaps packet with sequence number %d\n", count);
	memcpy(skb_push(new_skb, MYVPN_H_SZ), &myvpn_hdr, MYVPN_H_SZ);
	memcpy(skb_push(new_skb, UDP_H_SZ), &new_udp_hdr, UDP_H_SZ);
	memcpy(skb_push(new_skb, IP4_H_SZ), iph, IP4_H_SZ);
	iph = (struct iphdr *)(new_skb->data);
	iph->tot_len = cpu_to_be16(new_skb->len);
	iph->protocol = 17;
	iph->frag_off = 0; /* Avoids incorrect fragmentation of encrypted packet */
	iph->daddr = packet->peer->access_addr.s_addr;
	iph->saddr = new_skb->dev->ip_ptr->ifa_list->ifa_address;
	iph->check = 0;
	ip_send_check(iph);
	new_skb->ip_summed = CHECKSUM_UNNECESSARY;
	new_skb->network_header = new_skb->data - new_skb->head;
	if (new_skb != skb)
	{
		packet->skb = new_skb;
		dev_kfree_skb(skb);
	}
	return encrypt_and_auth(packet->peer->ctx_out,
							(u8 *)skb_myvpn_header(new_skb) + MYVPN_H_SZ,
							(u8 *)skb_myvpn_header(new_skb) + MYVPN_H_SZ,
							len, iv, skb_myvpn_header(new_skb)->cr_h.auth);
	/* It might be hard to encrypt/decrypt scatter-gathered data with cfb mode */
	/* blkcipher_walk_init, blkcipher_walk_virt_block, ..., blkcipher_walk_done*/
	/* OR : */
}
