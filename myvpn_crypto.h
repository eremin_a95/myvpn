#ifndef _MYVPN_CRYPTO_H
#define _MYVPN_CRYPTO_H

#define MYVPN_KEY_SZ 32
#define MYVPN_BLOCK_SZ 8
#define MYVPN_AUTH_SZ 8

#include "myvpn_packet.h"

#define ZEROKEY (u8 *)("\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0")

struct myvpn_crypto_ctx
{
	u8				key[MYVPN_KEY_SZ];
	u8				auth_key[MYVPN_KEY_SZ];
	u8				auth_k1[MYVPN_BLOCK_SZ], auth_k2[MYVPN_BLOCK_SZ];
	unsigned long	encrypted;
};

int test_crypto_system(void);

struct myvpn_crypto_ctx * create_crypto_ctx(const u8 * key,
											const u8 * auth_key	);
							
void destroy_crypto_ctx(struct myvpn_crypto_ctx * ctx);

void set_key(struct myvpn_crypto_ctx * ctx, const u8 * key);

void set_auth_key(struct myvpn_crypto_ctx * ctx, const u8 * auth_key);

int decrypt_and_auth(struct myvpn_crypto_ctx * ctx, const u8 * src, u8 * dst,
						int len, u64 iv, const u8 * auth);

int encrypt_and_auth(struct myvpn_crypto_ctx * ctx, const u8 * src, u8 * dst,
						int len, u64 iv, u8 * auth);
#endif