#ifndef _MYVPN_WORKING_H
#define _MYVPN_WORKING_H

#include <linux/interrupt.h>
#include <linux/percpu.h>
#include <linux/workqueue.h>

#include "myvpn_queueing.h"

typedef void (* task_func_t)(unsigned long);

struct myvpn_mc_container
{
	struct work_struct		work;
	struct myvpn_queue		* queue;
	int						cpu;
	struct myvpn_mc_work	* owner;
};

struct myvpn_mc_work
{
	struct myvpn_mc_container __percpu	* proc_work;
	struct tasklet_struct				* finish_task;
	int									cur_cpu, max_cpu, cpu_mask;
	spinlock_t							sl;
};

static inline
void queue_myvpn_finish_work(struct myvpn_mc_work * mc_work)
{
	tasklet_schedule(mc_work->finish_task);
}

struct myvpn_mc_work * create_myvpn_mc_work(work_func_t, task_func_t,
											struct myvpn_queue * queue,
											unsigned long data, gfp_t flags	);

void destroy_myvpn_mc_work(struct myvpn_mc_work * mc_work);

bool queue_myvpn_work(struct workqueue_struct * wq,
						struct myvpn_mc_work * mc_work	);
								
void free_cpu(int cpu, struct myvpn_mc_work * mc_work);

#endif