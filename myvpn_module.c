#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/inet.h>
#include <linux/delay.h>
#include <linux/timekeeping.h>

//#define MYVPN_DEBUG
#define IN_HOOK_NS_BARIER 5000
#define OUT_HOOK_NS_BARIER 5000


#include "myvpn_peer.h"
#include "myvpn_working.h"
#include "myvpn_crypto.h"

struct socket * sock;
struct nf_hook_ops input_bundle, output_bundle;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,1,0)
unsigned int input_hook (void * priv, struct sk_buff * skb,
							const struct nf_hook_state * state)
#else
unsigned int input_hook (unsigned int hooknum, struct sk_buff * skb,
							const struct net_device * in,
							const struct net_device * out,
							int (* okfn)(struct sk_buff *))
#endif
{
	if (is_myvpn_skb(skb))
	{
		struct myvpn_packet * packet;
		struct myvpn_peer * peer = peer_from_myvpn_skb(skb);
		if (unlikely(skb->len < MYVPN_OVERHEAD_SZ + IP4_H_SZ))
				return NF_DROP;
		packet = new_packet(skb, peer, MYVPN_STATUS_QUEUED, GFP_ATOMIC);
		if (likely(packet))
		{
			queue_packet(packet, peer->input_queue);
			queue_myvpn_work(peer->wq, peer->input_work);
			return NF_STOLEN;
		}
		else
			return NF_DROP;
	}
	else if (is_from_myvpn_skb(skb))
		return NF_ACCEPT; /* return encrypted_filter(skb) */
	else
		return NF_ACCEPT; /* return plaintext_filter(skb); */
		
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,1,0)
unsigned int output_hook (void * priv, struct sk_buff * skb,
							const struct nf_hook_state * state)
#else
unsigned int output_hook (unsigned int hooknum, struct sk_buff * skb,
							const struct net_device * in,
							const struct net_device * out,
							int (* okfn)(struct sk_buff *))
#endif
{
	if (is_myvpn_skb(skb))
		return NF_ACCEPT;
	else
	{
		struct myvpn_peer * peer = peer_from_plaintext_skb(skb);
		if (peer) /* if (peer && (encrypted_filter(skb) == NF_ACCEPT)) */
		{
			struct myvpn_packet * packet;
			if (unlikely(skb->len > MYVPN_PACKET_MAX_SZ))
				return NF_DROP;
			packet = new_packet(skb, peer, MYVPN_STATUS_QUEUED, GFP_ATOMIC);
			if (likely(packet))
			{
				queue_packet(packet, peer->output_queue);
				queue_myvpn_work(peer->wq, peer->output_work);
				return NF_STOLEN;
			}
			else
				return NF_DROP;
		}
		else
			return NF_ACCEPT; /* return plaintext_filter(skb); */
	}
}

static int __init myvpn_init(void)
{
	int err = 0;
	struct sockaddr_in addr;
	
	err = init_peers("/etc/myvpn/peers");
	if (err)
	{
		printk(KERN_ERR "MyVPN: can't init peers from %s (error: %d)!\n",
				"/etc/myvpn/peers", err);
		return -err;
	}
	
	sock_create_kern(&init_net, AF_INET, SOCK_DGRAM, IPPROTO_UDP, &sock);
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(local_peer()->peer_port);
	err = sock->ops->bind(sock,(struct sockaddr *)&addr,sizeof(struct sockaddr));
	/*if (err)
	{
		printk(KERN_ERR "MyVPN: can't bind socket to port %d (error: %d)\n",
				MYVPN_LOCAL_PORT, err);
		return -err;
	}*/
	err = test_crypto_system();
	if (err)
	{
		printk(KERN_ERR "MyVPN: crypto didn't pass the tests (error: %d)!\n",
				err);
		return -err;
	}
	
	if (!init_packet_allocator())
	{
		printk(KERN_INFO "MyVPN error: can't init packet allocator!\n");
		return -ENOMEM;
	}
		
	input_bundle.hook = input_hook;
	input_bundle.pf = PF_INET;
	input_bundle.hooknum = NF_INET_LOCAL_IN; //NF_INET_PRE_ROUTING
	input_bundle.priority = NF_IP_PRI_FIRST;
	
	output_bundle.hook = output_hook;
	output_bundle.pf = PF_INET;
	output_bundle.hooknum = NF_INET_POST_ROUTING;	
	/* output_bundle.hooknum = NF_INET_LOCAL_OUT; */
	/* is not with dev_queue_xmit */
	output_bundle.priority = NF_IP_PRI_FIRST;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,13,0)
	nf_register_net_hook(&init_net, &input_bundle);
	nf_register_net_hook(&init_net, &output_bundle);
#else
	nf_register_hook(&input_bundle);
	nf_register_hook(&output_bundle);
#endif
	
	printk(KERN_INFO "MyVPN kernel module init!\n");
	return(0);
}

static void __exit myvpn_exit(void)
{
	sock_release(sock);
	
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,13,0)
	nf_unregister_net_hook(&init_net, &input_bundle);
	nf_unregister_net_hook(&init_net, &output_bundle);
#else
	nf_unregister_hook(&input_bundle);
	nf_unregister_hook(&output_bundle);
#endif

	msleep(2000); /* To be sure that all tasks finished */

	uninit_peers();
	
	uninit_packet_allocator();
	
	printk(KERN_INFO "MyVPN kernel module exit!\n");	
}

module_init(myvpn_init)
module_exit(myvpn_exit);

MODULE_AUTHOR("Eremin Andrey eremin_a95@mail.ru");
MODULE_DESCRIPTION("My kernel multicore VPN module");
MODULE_LICENSE("GPL v2");