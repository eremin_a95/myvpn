#ifndef _MYVPN_QUEUEING_H
#define _MYVPN_QUEUEING_H

#include <linux/list.h>
#include <linux/spinlock.h>

#include "myvpn_packet.h"

enum {
	MYVPN_STATUS_DEFAULT = 0,
	MYVPN_STATUS_QUEUED = 1,
	MYVPN_STATUS_PROCESSING = 2,
	MYVPN_STATUS_READY = 3,
};

struct myvpn_queue
{
	struct list_head list;
	spinlock_t sl;
	atomic_t length;
};

struct myvpn_packet;

/** Creates queue */
struct myvpn_queue * create_queue(gfp_t flags);

/** Destroys queue */
void destroy_queue(struct myvpn_queue * queue);

/** Retruns true if queue is empty (locks queue while processing) */
bool is_queue_empty(struct myvpn_queue * queue);

/** Returns length of queue */
int queue_len(struct myvpn_queue * queue);

/** Adds packet to the tail of queue (locks queue while processing) */
void queue_packet(struct myvpn_packet * packet, struct myvpn_queue * queue);

/** Returns and removes packet from the beginning of queue with queue-lock */
struct myvpn_packet * dequeue_packet(struct myvpn_queue * queue);

/** Returns  packet, closest to beginning of list,
 * with status MYVPN_STATUS_QUEUED
 */
struct myvpn_packet * get_first_not_processed(struct myvpn_queue * queue);

/** Removes packet from queue */
void delete_packet_from_queue(struct myvpn_packet * packet,
								struct myvpn_queue	* queue	);

/** Returns and removes packet from the beginning of queue
 * with status MYVPN_STATUS_READY with locking queue
 */
struct myvpn_packet * dequeue_ready_packet(struct myvpn_queue * queue);

/** Returns and removes packet from the beginning of queue
 * wich has status MYVPN_STATUS_READY without locking queue
 */
struct myvpn_packet * __dequeue_ready_packet(struct myvpn_queue * queue);

/** Locks the queue */
static inline
void __lock_queue(struct myvpn_queue * queue)
{
	spin_lock_bh(&(queue->sl));
	/* With spin_lock_irq there is no retr in iperf3 TCP and no OUT OF ORDER
	 * in iperf3 UDP (and few losses), but ther is system crash with it */
}

/** Unlocks the queue */
static inline
void __unlock_queue(struct myvpn_queue * queue)
{
	spin_unlock_bh(&(queue->sl));
}

#endif

