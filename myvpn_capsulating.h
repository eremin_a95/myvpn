#ifndef _MYVPN_CAPSULATING_H
#define _MYVPN_CAPSULATING_H

#include <linux/ip.h>
#include <linux/etherdevice.h>

#include "myvpn_packet.h"
#include "myvpn_peer.h"
			
int decaps_packet(struct myvpn_packet * packet);

int encaps_packet(struct myvpn_packet * packet);

#endif