#include <linux/fs.h> 

#include "myvpn_peer.h"

#define PEER_NUM 2

struct __packed peer_storage
{
	__be32	id;
	__be16	port;
	u8		type;
	u8		real_addr_num;
	__be32	access_addr;
	__le32	tun_num;
	u8		in_key[MYVPN_KEY_SZ];
	u8		in_auth_key[MYVPN_KEY_SZ];
	u8		out_key[MYVPN_KEY_SZ];
	u8		out_auth_key[MYVPN_KEY_SZ];	
};

struct addr_hnode_container
{
	struct hlist_node	hnode;
	__be32				addr;
	struct myvpn_peer	* peer;
};

struct myvpn_peer * peers;
int peer_num;
struct workqueue_struct * myvpn_wq;
DECLARE_HASHTABLE(addr_ht, 16);
DECLARE_HASHTABLE(id_ht, 16);

void input_process(struct work_struct * work);
void output_process(struct work_struct * work);
void input_finish(unsigned long data);
void output_finish(unsigned long data);

static inline
struct myvpn_peer * lookup_peer_by_id(__be32 id)
{
	struct myvpn_peer * peer = NULL;
	hash_for_each_possible(id_ht, peer, id_hnode, id)
		if (peer->id == id)
			return peer;
	return NULL;
}

static inline
struct myvpn_peer * lookup_peer_by_ip(__be32 ip)
{
	struct addr_hnode_container * ct = NULL;
	hash_for_each_possible(addr_ht, ct, hnode, ip)
	{
		/* We only check if there is no hash-collision (address, we finding - ip,
		 *	is corresponds with address, we added to hash-table - ct->addr), if ip
		 * is not compares with ct->addr it means that there is hash-collision, we
		 * go for next hash-table entry with this hash
		 */
		if (ct->addr == ip)
			return ct->peer;
	}
	return NULL;
}

static inline
int init_peer_from_info(struct myvpn_peer * peer, struct peer_storage * ps)
{
	peer->id = ps->id;
	hash_add(id_ht, &peer->id_hnode, peer->id);
	peer->peer_port = ps->port;
	peer->local_port = peers[0].peer_port;
	peer->type = ps->type;
	peer->access_addr.s_addr = ps->access_addr;
	peer->r_addr_num = ps->real_addr_num;
	peer->r_addrs =
		kmalloc(peer->r_addr_num * sizeof(struct in_addr), GFP_KERNEL);
	if (peer->r_addrs == NULL)
		return ENOMEM;
	peer->tun_num = __le32_to_cpu(ps->tun_num);
	peer->tunnels =
		kmalloc(peer->tun_num * sizeof(struct in_addr), GFP_KERNEL);
	if (peer->tunnels == NULL)
		return ENOMEM;
	peer->mtu = 1500;
	peer->addr_hnode_ct = kmalloc(
		(peer->r_addr_num + peer->tun_num) *
			sizeof(struct addr_hnode_container), GFP_KERNEL	);
	if (peer->addr_hnode_ct == NULL)
		return ENOMEM;
	peer->ctx_in = create_crypto_ctx(ps->in_key, ps->in_auth_key);
	peer->ctx_out = create_crypto_ctx(ps->out_key, ps->out_auth_key);
	peer->input_queue = create_queue(GFP_KERNEL);
	peer->output_queue = create_queue(GFP_KERNEL);
	/* peer->index must be defined first */
	peer->input_work = create_myvpn_mc_work(&input_process, &input_finish,
		peer->input_queue, peer->index, GFP_KERNEL);
	peer->output_work = create_myvpn_mc_work(&output_process, &output_finish,
		peer->output_queue, peer->index, GFP_KERNEL);
	
	if (!(peer->ctx_in && peer->ctx_out && peer->input_queue &&
			peer->output_queue && peer->input_work && peer->output_work))
		return ENOMEM;
	return 0;
}

int init_peers(const char * filename)
{
	size_t len = 0;
	loff_t pos = 0;
	struct __packed {
		__le32	peer_num;
		u8		padding[12];
	} preambule;
	int cur_peer = 0;
	struct file * f; 
	struct peer_storage * peer_stor =
		kmalloc(sizeof(struct peer_storage), GFP_KERNEL);
	if (peer_stor == NULL)
		goto err_mem;
	
	/*myvpn_wq = alloc_workqueue("myvpn_wq",
									WQ_HIGHPRI,
									128);*/
	myvpn_wq = alloc_workqueue("myvpn_wq",
									WQ_CPU_INTENSIVE | WQ_HIGHPRI,
									128);
	/*myvpn_wq = alloc_workqueue("myvpn_wq",
									WQ_CPU_INTENSIVE | WQ_HIGHPRI | WQ_MEM_RECLAIM,
									128);*/
	if (myvpn_wq == NULL)
		goto err_wq;

	f = filp_open(filename, O_RDONLY, 0);
	if (f == NULL)
		goto err_file;
	len = kernel_read(f, &preambule, sizeof(preambule), &pos);
	if (len < sizeof(preambule))
		goto err_bad_peer_num;
	peer_num = __le32_to_cpu(preambule.peer_num);
	if (peer_num < 2)
		goto err_bad_peer_num;
	peers = kmalloc(peer_num * sizeof(struct myvpn_peer), GFP_KERNEL);
	if (peers == NULL)
	{
		kfree(peer_stor);
		filp_close(f, 0);
		return ENOMEM;
	}
	memset(peers, 0, peer_num * sizeof(struct myvpn_peer));
	hash_init(addr_ht);
	hash_init(id_ht);
	while (peer_num--)
	{
		int addr_num, tun_num, cur_addr = 0;
		len = kernel_read(f, peer_stor, sizeof(struct peer_storage), &pos);
		if (len < sizeof(struct peer_storage))
			goto err_bad_peer_struct;
		peers[cur_peer].index = cur_peer;
		peers[cur_peer].wq = myvpn_wq;
		if (init_peer_from_info(&peers[cur_peer], peer_stor))
			goto err_bad_peer_struct;
		addr_num = peer_stor->real_addr_num;
		tun_num = __le32_to_cpu(peer_stor->tun_num);
		while (addr_num--)
		{
			len = kernel_read(f, &(peers[cur_peer].r_addrs[cur_addr].s_addr),
								sizeof(u32), &pos);
			if (len != sizeof(u32))
				goto err_bad_peer_struct;
			peers[cur_peer].addr_hnode_ct[cur_addr].peer = &peers[cur_peer];
			peers[cur_peer].addr_hnode_ct[cur_addr].addr =
				peers[cur_peer].r_addrs[cur_addr].s_addr;
			hash_add(addr_ht, &peers[cur_peer].addr_hnode_ct[cur_addr].hnode,
						peers[cur_peer].r_addrs[cur_addr].s_addr);
			cur_addr++;
		}
		addr_num = peer_stor->real_addr_num;
		while (tun_num--)
		{
			len = kernel_read(f,
				&(peers[cur_peer].tunnels[cur_addr - addr_num].s_addr),
				sizeof(u32), &pos);
			if (len != sizeof(u32))
				goto err_bad_peer_struct;
			peers[cur_peer].addr_hnode_ct[cur_addr].peer = &peers[cur_peer];
			peers[cur_peer].addr_hnode_ct[cur_addr].addr =
				peers[cur_peer].tunnels[cur_addr - addr_num].s_addr;
			hash_add(addr_ht, &peers[cur_peer].addr_hnode_ct[cur_addr].hnode,
						peers[cur_peer].tunnels[cur_addr - addr_num].s_addr);
			cur_addr++;
		}
		cur_peer++;
		if (16 - (((cur_addr) * 4) % 16) != 16) /* If there is padding after peer */
		{
			len = kernel_read(f, &preambule, 16 - (((cur_addr) * 4) % 16), &pos);
			if (len != (16 - (((cur_addr) * 4) % 16)))
				goto err_bad_peer_struct;
		}
	}
	peer_num = __le32_to_cpu(preambule.peer_num);
	kfree(peer_stor);
	filp_close(f, 0);
	return 0;
err_wq:
	kfree(peer_stor);
err_mem:
	return ENOMEM;
err_file:
	kfree(peer_stor);
	return ENOENT;
err_bad_peer_num:
	kfree(peer_stor);
	filp_close(f, 0);
	return EIO;
err_bad_peer_struct:
	kfree(peer_stor);
	kfree(peers);
	filp_close(f, 0);
	return EIO;
}

void uninit_peers(void)
{
	while (peer_num--)
	{
		destroy_myvpn_mc_work(peers[peer_num].input_work);
		destroy_myvpn_mc_work(peers[peer_num].output_work);		
		destroy_queue(peers[peer_num].input_queue);
		destroy_queue(peers[peer_num].output_queue);
		destroy_crypto_ctx(peers[peer_num].ctx_in);
		destroy_crypto_ctx(peers[peer_num].ctx_out);
		kfree(peers[peer_num].addr_hnode_ct);
		if (peers[peer_num].r_addr_num)
			kfree(peers[peer_num].r_addrs);
		if (peers[peer_num].tun_num)
			kfree(peers[peer_num].tunnels);
	}
	kfree(peers);
	destroy_workqueue(myvpn_wq); /* GPL!!! */
	return;	
}

struct myvpn_peer * local_peer(void)
{
	return &peers[0];
}

struct myvpn_peer * get_peer(unsigned long index)
{
	return &peers[index];
}

struct myvpn_peer * peer_from_myvpn_skb(struct sk_buff * skb)
{
	return lookup_peer_by_id(skb_myvpn_header(skb)->src_id);
}

struct myvpn_peer * peer_from_plaintext_skb(struct sk_buff * skb)
{
	struct addr_hnode_container * ct = NULL;
	struct iphdr * iph = ip_hdr(skb);
	hash_for_each_possible(addr_ht, ct, hnode, iph->saddr)
	{
		int addr_num = ct->peer->r_addr_num;
		if (unlikely(ct->peer != local_peer()))
			continue;
		while (addr_num--)
			if (ct->peer->r_addrs[addr_num].s_addr == iph->saddr)
				return lookup_peer_by_ip(iph->daddr);
		addr_num = ct->peer->tun_num;
		while (addr_num--)
			if (ct->peer->tunnels[addr_num].s_addr == iph->saddr)
				return lookup_peer_by_ip(iph->daddr);
	}
	return false;
}

static inline
void __update_peer(struct myvpn_peer * peer, __be32 addr,
					__be16 peer_port, __be16 local_port, int mtu)
{
	if (addr)
		peer->access_addr.s_addr = addr;
	if (peer_port)
		peer->peer_port = peer_port;
	if (local_port)
		peer->local_port = local_port;
	if (mtu)
		peer->mtu = mtu;						
}

void update_peer(	struct myvpn_peer * peer, struct sk_buff * skb)
{
	if (likely((skb->data_len > MYVPN_OVERHEAD_SZ) || (!skb->data_len)))
	{
		struct iphdr * iph = ip_hdr(skb);
		struct udphdr * udph = udp_hdr(skb);
		__update_peer(peer, iph->saddr, udph->source, udph->dest, 0);		
	}
	else
	{
		struct iphdr iph;
		struct udphdr udph;
		skb_copy_bits(skb, skb_network_offset(skb), &iph, IP4_H_SZ);
		skb_copy_bits(skb, skb_transport_offset(skb), &udph, UDP_H_SZ);
		__update_peer(peer, iph.saddr, udph.source, udph.dest, 0);
	}
}

bool is_skb_for_peer(struct sk_buff * skb, struct myvpn_peer * peer)
{
	struct addr_hnode_container * ct = NULL;
	struct iphdr * iph = ip_hdr(skb);
	hash_for_each_possible(addr_ht, ct, hnode, iph->daddr)
	{
		/* Is those checks necessary? */
		int addr_num = ct->peer->r_addr_num;
		if (unlikely(ct->peer != peer))
			continue;
		while (addr_num--)
			if (ct->peer->r_addrs[addr_num].s_addr == iph->daddr)
				return true;
	}
	return false;
}

bool is_skb_for_tunnel(struct sk_buff * skb, struct myvpn_peer * peer)
{
	struct addr_hnode_container * ct = NULL;
	struct iphdr * iph = ip_hdr(skb);
	hash_for_each_possible(addr_ht, ct, hnode, iph->daddr)
	{
		/* Is those checks necessary? */
		int addr_num = ct->peer->tun_num;
		if (unlikely(ct->peer != peer))
			continue;
		while (addr_num--)
			if (ct->peer->tunnels[addr_num].s_addr == iph->daddr)
				return true;
	}
	return false;
}

