#ifndef _MYVPN_PACKET_H
#define _MYVPN_PACKET_H

#include <linux/ip.h>
#include <linux/list.h>

#include "myvpn_crypto.h"
#include "myvpn_peer.h"

#define MYVPN_SIGN (u32)(0x26594131)
#define MYVPN_SIGN_SZ 4
#define ETH_H_SZ 14
#define IP4_H_SZ 20
#define UDP_H_SZ 8
#define MAX_MAC_H_SZ 48
#define MYVPN_H_SZ (sizeof(struct myvpn_header))
#define MYVPN_OVERHEAD_SZ (IP4_H_SZ + UDP_H_SZ + MYVPN_H_SZ)
#define MYVPN_OVERTAIL_SZ (0)
#define MYVPN_PACKET_MAX_SZ 65400

enum {
	MYVPN_UNKNOWN = 0,
	MYVPN_TO_HOST = 1,
	MYVPN_TO_TUNNEL = 2,
};

struct __packed crypto_header {
	union	{
		u8	iv[MYVPN_BLOCK_SZ];
		u64	iv64;
	};
	union	{
		u8	auth[MYVPN_AUTH_SZ];
		u64	auth64;
	};
};

struct __packed myvpn_header {
	u32						sign;
	__be32					src_id;
	__be32					dst_id;
	u32						seq_num; /* TODO: remove */
	u8						reserved[12];
	struct crypto_header	cr_h;
};

struct myvpn_packet {
	struct sk_buff			* skb;
	struct myvpn_peer		* peer;
	struct list_head		list;
	int						status;
	int						type;
	struct crypto_header	cr_h; /* TODO: remove */
};

bool init_packet_allocator(void);

void uninit_packet_allocator(void);

bool is_myvpn_skb(struct sk_buff * skb);

bool is_from_myvpn_skb(struct sk_buff * skb);

struct myvpn_packet * new_packet(struct sk_buff * skb,
									struct myvpn_peer * peer,
									int status, gfp_t flags);
											
void destroy_packet(struct myvpn_packet * packet);

static inline
struct myvpn_header * skb_myvpn_header(struct sk_buff * skb)
{
	return (struct myvpn_header *)(skb->data + MYVPN_OVERHEAD_SZ - MYVPN_H_SZ);
}

static inline
int skb_myvpn_offset(struct sk_buff * skb)
{
	return (u8 *)skb_myvpn_header(skb) - skb->data;
}

#endif