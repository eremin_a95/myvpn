#include "myvpn_crypto.h"
#include "crypto/magma.c"

struct myvpn_crypto_ctx * create_crypto_ctx(const u8 * key,
											const u8 * auth_key)
{
	struct myvpn_crypto_ctx * ctx =
		kmalloc(sizeof(struct myvpn_crypto_ctx), GFP_KERNEL);
	if (ctx == NULL)
		return NULL;
	memcpy(ctx->key, key, MYVPN_KEY_SZ);
	memcpy(ctx->auth_key, auth_key, MYVPN_KEY_SZ);
	magma_make_auth_subkeys(ctx->auth_key, ctx->auth_k1, ctx->auth_k2);
	ctx->encrypted = 0;
	return ctx;
}

void destroy_crypto_ctx(struct myvpn_crypto_ctx * ctx)
{
	/* TODO: How to be sure that memory will be cleaned? */
	memset(ctx->key, 0, MYVPN_KEY_SZ);
	memset(ctx->auth_key, 0, MYVPN_KEY_SZ);
	memset(ctx->auth_k1, 0, MYVPN_BLOCK_SZ);
	memset(ctx->auth_k2, 0, MYVPN_BLOCK_SZ);
	kfree(ctx);
}

int decrypt_and_auth(struct myvpn_crypto_ctx * ctx, const u8 * src, u8 * dst,
							int len, u64 iv, const u8 * auth)
{
	ctx->encrypted += len;
	return magma_cfb_decrypt_auth(src, dst, auth, len, ctx->key, iv,
									ctx->auth_key, ((u64 *)ctx->auth_k1)[0],
									((u64 *)ctx->auth_k2)[0], MYVPN_AUTH_SZ);
}

int encrypt_and_auth(struct myvpn_crypto_ctx * ctx, const u8 * src, u8 * dst,
						int len, u64 iv, u8 * auth)
{
	ctx->encrypted += len;
	return magma_cfb_encrypt_auth(src, dst, auth, len, ctx->key, iv,
									ctx->auth_key, ((u64 *)ctx->auth_k1)[0],
									((u64 *)ctx->auth_k2)[0], MYVPN_AUTH_SZ);
}

#define PRINT_VEC8(vec) \
	printk(KERN_INFO #vec ": %02x%02x%02x%02x%02x%02x%02x%02x\n", \
		(vec)[0], (vec)[1], (vec)[2], (vec)[3], (vec)[4], (vec)[5],\
		(vec)[6], (vec)[7]);
#define PRINT_VEC4(vec) \
	printk(KERN_INFO #vec ": %02x%02x%02x%02x\n", \
		(vec)[0], (vec)[1], (vec)[2], (vec)[3]);

int test_crypto_system(void)
{
	struct myvpn_crypto_ctx * ctx;
	const u8 testkey[32] = {
		0xff,0xfe,0xfd,0xfc,0xfb,0xfa,0xf9,0xf8,
		0xf7,0xf6,0xf5,0xf4,0xf3,0xf2,0xf1,0xf0,
		0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,
		0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff,
	};
	const u8 plaintext[8] =		{0x10,0x32,0x54,0x76,0x98,0xba,0xdc,0xfe};
	const u8 ciphertext[8] =	{0x3d,0xca,0xd8,0xc2,0xe5,0x01,0xe9,0x4e};
	const u8 r[8] =				{0x12,0x0a,0x29,0xa1,0x99,0xcd,0xa2,0x2f};
	const u8 k1[8] =			{0x24,0x14,0x52,0x42,0x33,0x9b,0x45,0x5f};
	const u8 k2[8] =			{0x48,0x28,0xa4,0x84,0x66,0x36,0x8b,0xbe};
	const u8 mac[4] =			{0x10,0x72,0x4e,0x15};
	const u8 plt[32] = {
		0x59,0x0a,0x13,0x3c,0x6b,0xf0,0xde,0x92,
		0x20,0x9d,0x18,0xf8,0x04,0xc7,0x54,0xdb,
		0x4c,0x02,0xa8,0x67,0x2e,0xfb,0x98,0x4a,
		0x41,0x7e,0xb5,0x17,0x9b,0x40,0x12,0x89,
	};

	u8 vec[32];
	u8 testvec[8];
	
	/* Test encryption */
	memcpy(testvec, plaintext, 8);
	magma_block_encrypt(testvec, testvec, testkey);
	if (memcmp(testvec, ciphertext, 8))
	{
		PRINT_VEC8(testvec);
		PRINT_VEC8(ciphertext);
		return 201;
	}
	/* --------------- */
	/* Test decryption */
	memcpy(testvec, ciphertext, 8);
	magma_block_decrypt(testvec, testvec, testkey);
	if (memcmp(testvec, plaintext, 8))
	{
		PRINT_VEC8(testvec);
		PRINT_VEC8(plaintext);
		return 202;
	}
	/* --------------- */
	ctx = create_crypto_ctx(testkey, testkey);
	/* Test CFB mode */
	/* TODO: test CFB */
	/* --------------- */
	/* Test Imito mode */
	memset(testvec, 0, 8);
	magma_block_encrypt(testvec, testvec, testkey);
	if (memcmp(testvec, r, 8))
	{
		PRINT_VEC8(testvec);
		PRINT_VEC8(r);
		return 206;
	}
	if (memcmp(&ctx->auth_k1, &k1, 8))
	{
		PRINT_VEC8((u8 *)&ctx->auth_k1);
		PRINT_VEC8((u8 *)&k1);
		return 207;
	}
	if (memcmp(&ctx->auth_k2, &k2, 8))
	{
		PRINT_VEC8((u8 *)&ctx->auth_k2);
		PRINT_VEC8((u8 *)&k2);
		return 208;
	}
	magma_cfb_encrypt_auth(plt, vec, testvec, 32, ctx->key, 0, ctx->auth_key,
								 ((u64 *)ctx->auth_k1)[0], ((u64 *)ctx->auth_k2)[0], 4);
	if (memcmp(testvec, mac, 4))
	{
		PRINT_VEC4(testvec);
		PRINT_VEC4(mac);
		return 209;
	}
	if (magma_cfb_decrypt_auth(vec, vec, testvec, 32, ctx->key, 0, ctx->auth_key,
								 ((u64 *)ctx->auth_k1)[0], ((u64 *)ctx->auth_k2)[0], 4))
		return 210;
	/* --------------- */
	/* TODO: test others */
	destroy_crypto_ctx(ctx);
	return 0;
}