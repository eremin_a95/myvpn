#include "myvpn_queueing.h"

struct myvpn_queue * create_queue(gfp_t flags)
{
	struct myvpn_queue * queue = kmalloc(sizeof(struct myvpn_queue), flags);
	atomic_set(&queue->length, 0);
	spin_lock_init(&(queue->sl));
	INIT_LIST_HEAD(&(queue->list));
	return queue;
}

void destroy_queue(struct myvpn_queue * queue)
{
	kfree(queue);
}

/** Retruns true if queue is empty (not locks queue while processing) */
static inline
bool __is_queue_empty(struct myvpn_queue * queue)
{
	return !atomic_read(&queue->length);
}

bool is_queue_empty(struct myvpn_queue * queue)
{
	return __is_queue_empty(queue);
}

int queue_len(struct myvpn_queue * queue)
{
	return atomic_read(&queue->length);
}

/** Adds packet into the end of queue without queue-lock */
static inline
void __queue_packet(struct myvpn_packet * packet, struct myvpn_queue * queue)
{
	packet->status = MYVPN_STATUS_QUEUED;
	list_add_tail(&(packet->list), &(queue->list));
	atomic_inc(&queue->length);
}

void queue_packet(struct myvpn_packet * packet, struct myvpn_queue * queue)
{
	/* TODO: maybe we can avoid lock/unlock here because of atomic len field? */
	__lock_queue(queue);
	__queue_packet(packet, queue);
	__unlock_queue(queue);	
}

/** Returns and removes packet from the beginning of
 * queue without queue-lock
 */
static inline
struct myvpn_packet * __dequeue_packet(struct myvpn_queue * queue)
{
	/* TODO: check queue->length instead of check packet == NULL */
	if (unlikely(__is_queue_empty(queue)))
		return NULL;
	else
	{
		struct myvpn_packet * packet =
			list_first_entry(&(queue->list), struct myvpn_packet, list);
		atomic_dec(&queue->length);
		list_del(&(packet->list)); /* Or list_del_init needed? */
		return packet;
	}
}

struct myvpn_packet * dequeue_packet(struct myvpn_queue * queue)
{
	struct myvpn_packet * packet;
	__lock_queue(queue);
	packet = __dequeue_packet(queue);
	__unlock_queue(queue);
	return packet;
}

struct myvpn_packet * get_first_not_processed(struct myvpn_queue * queue)
{
	struct myvpn_packet * entry, * packet = NULL;
	struct list_head * ptr;
	__lock_queue(queue);
	list_for_each(ptr, &(queue->list))
	{
		entry = list_entry(ptr, struct myvpn_packet, list);
		if (entry->status == MYVPN_STATUS_QUEUED)
		{
			packet = entry;
			packet->status = MYVPN_STATUS_PROCESSING;
			break;
		}
	}
	__unlock_queue(queue);
	return packet;
}

void delete_packet_from_queue(struct myvpn_packet * packet,
								struct myvpn_queue * queue)
{
	__lock_queue(queue);
	atomic_dec(&queue->length);
	list_del(&(packet->list));	
	__unlock_queue(queue);
}

struct myvpn_packet * dequeue_ready_packet(struct myvpn_queue * queue)
{
	struct myvpn_packet * entry, * packet = NULL;
	__lock_queue(queue);
	if (unlikely(list_empty(&(queue->list))))
		goto unlock;
	entry = list_first_entry(&(queue->list), struct myvpn_packet, list);
	if (entry->status == MYVPN_STATUS_READY)
	{		
		packet = entry;
		atomic_dec(&queue->length);
		list_del(&(packet->list));
	}
unlock:
	__unlock_queue(queue);
	return packet;
}

struct myvpn_packet * __dequeue_ready_packet(struct myvpn_queue * queue)
{
	struct myvpn_packet * entry, * packet = NULL;
	if (unlikely(list_empty(&(queue->list))))
		return NULL;
	entry = list_first_entry(&(queue->list), struct myvpn_packet, list);
	if (entry->status == MYVPN_STATUS_READY)
	{
		packet = entry;
		atomic_dec(&queue->length);
		list_del(&(packet->list));
	}
	return packet;
}
