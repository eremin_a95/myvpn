-- trivial protocol example
-- declare our protocol
myvpn_proto = Proto("MyVPN","MyVPN Protocol")


-- create a function to dissect it
function myvpn_proto.dissector(buffer,pinfo,tree)
	pinfo.cols.protocol = "MyVPN"
	local subtree = tree:add(myvpn_proto,buffer(), "MyVPN Protocol Data")
	local std_header = 0
	local data_length = buffer:len() - 44

	--MyVPN sign
	subtree:add(buffer(std_header, 4), "MyVPN signature: " .. buffer(std_header, 4))

	--src node
	subtree:add(buffer(std_header+4, 4), "Source node: " .. buffer(std_header+4, 4))
	
	--dst node
	subtree:add(buffer(std_header+8, 4), "Destination node: " .. buffer(std_header+8, 4))
	
	--seq num
	subtree:add(buffer(std_header+12,4), "Sequence number: " .. buffer(std_header+12,4):uint())

	--reserved
	subtree:add(buffer(std_header+16,12), "Reserved (" .. buffer(std_header+16,12) .. ")" )

	--IV
	subtree:add(buffer(std_header+28,8), "IV: " .. buffer(std_header+28,8))
	
	--Auth tag
	subtree:add(buffer(std_header+36,8), "Auth tag: " .. buffer(std_header+36,8))

	--encrypted data
	--subtree:add(buffer(std_header+44,data_length), "Encrypted data (" .. data_length .. " bytes): " .. buffer(std_header+44, data_length))

end


-- load the udp.port table
udp_table = DissectorTable.get("udp.port")
-- register our protocol to handle udp port 12345
udp_table:add(12345, myvpn_proto)
